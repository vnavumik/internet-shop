package servlets;


import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(ExceptionServlet.class);

    @Override
    public void init() {
        logger.info("ExceptionServlet init!");
    }

    @Override
    public void destroy() {
        logger.info("ExceptionServlet destroy!");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* Analyze The Servlet Exception */

        Throwable throwable = (Throwable) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
        Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        String servletName = (String) request.getAttribute(RequestDispatcher.ERROR_SERVLET_NAME);
        if (servletName == null) {
            servletName = "Unknown";
        }
        String requestUri = (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
        if (requestUri == null) {
            requestUri = "Unknown";
        }

        /* Print The Servlet Exception */
        logger.info("Error information");
        logger.info("The status code: " + statusCode);
        logger.info("Servlet Name: " + servletName);
        logger.info("Exception Type: " + throwable.getClass().getName());
        logger.info("The request URI: " + requestUri);
        //throwable.printStackTrace();
        logger.info(throwable.getMessage(),throwable);

        /* Set  attribute  */
        request.setAttribute("the_status_code", statusCode);
        request.setAttribute("servlet_name", servletName);
        request.setAttribute("exception_type", throwable.getClass().getName());
        request.setAttribute("the_request_URI", requestUri);

        /* Forward  to errors.jsp */
        String page = ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.ERRORS_PAGE_PATH);
        getServletContext().getRequestDispatcher(page).forward(request, response);
    }
}
