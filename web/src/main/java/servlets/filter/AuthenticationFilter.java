package servlets.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import servlets.model.CommandEnum;
import servlets.model.RolePrincipalEnum;
import servlets.model.UserPrincipal;

public class AuthenticationFilter implements Filter {

    private static final Set<CommandEnum> USER_AVAILABLE = new HashSet<>();
    private static final Set<CommandEnum> ADMIN_AVAILABLE = new HashSet<>();
    private static final String LOGIN_PATH = "/index.jsp";
    private static final Logger logger = LogManager.getLogger(AuthenticationFilter.class);


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("AuthenticationFilter initialized");

        USER_AVAILABLE.add(CommandEnum.USER);
        USER_AVAILABLE.add(CommandEnum.PRODUCTSFORUSER);
        USER_AVAILABLE.add(CommandEnum.LOGIN);
        USER_AVAILABLE.add(CommandEnum.ORDERSFORUSER);
        USER_AVAILABLE.add(CommandEnum.DELETEORDERBYUSER);
        USER_AVAILABLE.add(CommandEnum.ADDORDER);
        USER_AVAILABLE.add(CommandEnum.ADDFEEDBACK);
        USER_AVAILABLE.add(CommandEnum.GOFEEDBACKPAGE);
        ADMIN_AVAILABLE.add(CommandEnum.ADMIN);
        ADMIN_AVAILABLE.add(CommandEnum.SHOWUSERS);
        ADMIN_AVAILABLE.add(CommandEnum.PRODUCTSFORADMIN);
        ADMIN_AVAILABLE.add(CommandEnum.SHOWORDERS);
        ADMIN_AVAILABLE.add(CommandEnum.ADDPRODUCTS);
        ADMIN_AVAILABLE.add(CommandEnum.DELETEUSER);
        ADMIN_AVAILABLE.add(CommandEnum.DELETEORDERBYADMIN);
        ADMIN_AVAILABLE.add(CommandEnum.DELETEPRODUCT);
        ADMIN_AVAILABLE.add(CommandEnum.LOGIN);
        ADMIN_AVAILABLE.add(CommandEnum.SHOWFEEDBACKS);
        ADMIN_AVAILABLE.add(CommandEnum.AUDITSFORADMIN);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        String command = req.getParameter("command");
        if (session == null) {
            defaultRequest(request, response, chain, req, res, command);
        } else {
            UserPrincipal user = (UserPrincipal) session.getAttribute("user");
            if (user == null) {
                defaultRequest(request, response, chain, req, res, command);
            } else {
                CommandEnum commandEnum = CommandEnum.getCommand(command);
                RolePrincipalEnum role = user.getRole();
                switch (role) {
                    case USER:
                        if (USER_AVAILABLE.contains(commandEnum)) {
                            chain.doFilter(request, response);
                        } else {
                            session.removeAttribute("user");
                            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        }
                        break;
                    case ADMIN:
                        if (ADMIN_AVAILABLE.contains(commandEnum)) {
                            chain.doFilter(request, response);
                        } else {
                            session.removeAttribute("user");
                            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        }
                        break;
                    default:
                        session.removeAttribute("user");
                        res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        break;
                }
            }
        }
    }

    private void defaultRequest(ServletRequest request, ServletResponse response, FilterChain chain, HttpServletRequest req, HttpServletResponse res, String command) throws IOException, ServletException {
        if (req.getMethod().equals("POST")) {
            if (CommandEnum.getCommand(command) == CommandEnum.LOGIN) {
                chain.doFilter(request, response);
            } else {
                res.sendRedirect(req.getContextPath() + LOGIN_PATH);
            }
        } else {
            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
        }
    }

    @Override
    public void destroy() {
    }
}