package servlets.util;

import ru.inbox.vitnau.dao.model.User;
import servlets.model.UserPrincipal;

public class UserPrincipalConverter {
    public static UserPrincipal toUserPrincipal(User user) {
        return UserPrincipal.newBuilder().
                withId(user.getId()).
                withName(user.getFirstName() + " " + user.getLastName()).withEmail(user.getEmail()).
                withRole(user.getRole().name()).
                build();
    }
}
