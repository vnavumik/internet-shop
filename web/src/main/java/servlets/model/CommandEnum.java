package servlets.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum CommandEnum {
    LOGIN,
    SHOWUSERS,
    USER,
    ADMIN,
    AUDITSFORADMIN,
    GOFEEDBACKPAGE,
    PRODUCTSFORADMIN,
    PRODUCTSFORUSER,
    SHOWORDERS,
    SHOWFEEDBACKS,
    ADDFEEDBACK,
    ORDERSFORUSER,
    ADDPRODUCTS,
    ADDORDER,
    DELETEUSER,
    DELETEORDERBYADMIN,
    DELETEORDERBYUSER,
    DELETEPRODUCT;

    private static final Logger logger = LogManager.getLogger(CommandEnum.class);

    public static CommandEnum getCommand(String command) {
        try {
            return CommandEnum.valueOf(command.toUpperCase());

        } catch (IllegalArgumentException e) {
            logger.info("Command does not found!");
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
