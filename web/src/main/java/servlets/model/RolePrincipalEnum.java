package servlets.model;

public enum RolePrincipalEnum {
    USER("USER"),
    ADMIN("ADMIN");

    private final String description;

    RolePrincipalEnum(String description) {
        this.description = description;
    }
}