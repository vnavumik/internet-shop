package servlets.model;


import java.io.Serializable;

public class UserPrincipal implements Serializable {

    private Long id;
    private String name;
    private String email;
    private RolePrincipalEnum role;


    @Override
    public String
    toString() {
        return "UserPrincipal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role.name() +
                '}';
    }


    private UserPrincipal(Builder builder) {
        id = builder.id;
        name = builder.name;
        email = builder.email;
        role = builder.role;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public RolePrincipalEnum getRole() {
        return role;
    }

    public static Builder newBuilder() {

        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String email;
        private RolePrincipalEnum role;

        public Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withRole(String val) {
            if (RolePrincipalEnum.USER.name().equals(val)) {
                role = RolePrincipalEnum.USER;
            }
            if (RolePrincipalEnum.ADMIN.name().equals(val)) {
                role = RolePrincipalEnum.ADMIN;
            }
            return this;
        }

        public UserPrincipal build() {
            return new UserPrincipal(this);
        }
    }
}
