package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Order;
import ru.inbox.vitnau.OrderService;
import ru.inbox.vitnau.impl.OrderServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

public class ShowOrdersForAdminCommand implements Command {
    private OrderService orderService = new OrderServiceImpl();
    private static final Logger logger = LogManager.getLogger(ShowOrdersForAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long id = userPrincipal.getId();
        logger.info("Request from Admin with id=" + id + " by ShowOrdersForAdminCommand to show all orders");
        List<Order> orderList = orderService.getAll();
        request.setAttribute("orders", orderList);
        logger.info("Admin with id=" + id + " has received by ShowOrdersForAdminCommand following orders " +
                "for his request:");
        BigDecimal totalCost = BigDecimal.valueOf(0);
        for (Order element : orderList) {
            logger.info("orderId=" + element.getId());
            totalCost = totalCost.add(element.getProduct().getPrice());
        }
        request.setAttribute("total_cost", totalCost);
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.ORDERS_PAGE_PATH);
    }
}
