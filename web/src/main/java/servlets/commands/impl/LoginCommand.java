package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import ru.inbox.vitnau.dao.model.User;
import ru.inbox.vitnau.UserService;
import ru.inbox.vitnau.impl.UserServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.util.UserPrincipalConverter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginCommand implements Command {

    private UserService userService = new UserServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();// new
        session.removeAttribute("user");//new
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (email != null && !email.equals("")) {
            User userByUserName = userService.getByEmail(email);
            if (userByUserName != null) {
                if (userByUserName.getPassword().equals(password.trim())) {
                    session.setAttribute("user", UserPrincipalConverter.toUserPrincipal(userByUserName));

                    switch (userByUserName.getRole()) {
                        case USER:
                            response.sendRedirect("/dispatcher?command=" + CommandEnum.USER.name().toLowerCase());
                            break;
                        case ADMIN:
                            response.sendRedirect("/dispatcher?command=" + CommandEnum.ADMIN.name().toLowerCase());
                            break;
                        default:
                            response.sendRedirect("/dispatcher?command=" + CommandEnum.LOGIN.name().toLowerCase());
                            break;
                    }
                    return null;
                } else {
                    request.setAttribute("error", "Email or password is not correct!");
                    return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.LOGIN_PAGE_PATH);
                }
            } else {
                request.setAttribute("error", "Email  or password is not correct!");
                return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.LOGIN_PAGE_PATH);
            }
        } else {
            return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.LOGIN_PAGE_PATH);
        }
    }
}




