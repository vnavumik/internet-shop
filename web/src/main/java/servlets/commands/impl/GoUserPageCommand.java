package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import servlets.commands.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GoUserPageCommand implements Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) {
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.USER_PAGE_PATH);
    }
}
