package servlets.commands.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.UserService;
import ru.inbox.vitnau.impl.UserServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteUserByAdminCommand implements Command {

    private UserService userService = new UserServiceImpl();
    private static final Logger logger = LogManager.getLogger(DeleteUserByAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        String[] userIds = request.getParameterValues("ids");
        int length = userIds.length;
        if (length != 0) {
            for (int i = 0; i < length; i++) {
                logger.info("Request from 'users.jsp' page from Admin with userId=" + userId +
                        " by DeleteUserByAdminCommand to delete User with userId=" + userIds[i]);
                userService.deleteById(Long.valueOf(userIds[i]));
            }
        }
        logger.info("DeleteUserByAdminCommand redirect to ShowUsersForAdminCommand for Admin" +
                " with userId=" + userId);
        respond.sendRedirect("/dispatcher?command=" + CommandEnum.SHOWUSERS.name().toLowerCase());

        return null;
    }
}
