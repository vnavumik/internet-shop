package servlets.commands.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.ProductService;
import ru.inbox.vitnau.impl.ProductServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteProductByAdminCommand implements Command {
    private ProductService productService = new ProductServiceImpl();
    private static final Logger logger = LogManager.getLogger(DeleteProductByAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        String[] productIds = request.getParameterValues("ids");
        int length = productIds.length;
        if (length != 0) {
            for (int i = 0; i < length; i++) {
                logger.info("Request from 'productsForAdmin.jsp' page from Admin with userId=" + userId +
                        " by DeleteProductByAdminCommand to delete product with productId=" + productIds[i]);
                productService.deleteById(Long.valueOf(productIds[i]));
            }
        }
        logger.info("DeleteProductByAdminCommand redirect to ShowProductsForAdminCommand for Admin" +
                " with userId=" + userId);
        respond.sendRedirect("/dispatcher?command=" + CommandEnum.PRODUCTSFORADMIN.name().toLowerCase());

        return null;
    }
}
