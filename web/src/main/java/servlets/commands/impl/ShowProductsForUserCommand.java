package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.ProductService;
import ru.inbox.vitnau.impl.ProductServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowProductsForUserCommand implements Command {

    private ProductService productService = new ProductServiceImpl();
    private static final Logger logger = LogManager.getLogger(ShowProductsForUserCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        logger.info("Request from  User with userId=" + userId + " by ShowProductsForUserCommand" +
                " to show all products");
        List<Product> products = productService.getAll();
        logger.info("User with userId=" + userId + " has received by ShowProductsForUserCommand following" +
                " products for his request to show all products:");
        for (Product element : products) {
            logger.info("productId=" + element.getId());
        }
        request.setAttribute("products", products);
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.PRODUCTS_USER_PAGE_PATH);
    }
}
