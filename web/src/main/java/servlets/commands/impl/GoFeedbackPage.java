package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import servlets.commands.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoFeedbackPage implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.USER_FEEDBACK_PAGE_PATH);
    }
}
