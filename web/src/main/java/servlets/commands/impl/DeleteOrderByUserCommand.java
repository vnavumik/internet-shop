package servlets.commands.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.OrderService;
import ru.inbox.vitnau.impl.OrderServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteOrderByUserCommand implements Command {

    private OrderService orderService = new OrderServiceImpl();
    private static final Logger logger = LogManager.getLogger(DeleteOrderByUserCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        String[] ids = request.getParameterValues("ids");
        Long userId = userPrincipal.getId();
        if (ids != null) {
            for (String id : ids) {
                logger.info("Request from 'ordersForUser.jsp' page by DeleteOrderByUserCommand" +
                        " to delete by User with userId=" + userId + " order with orderId:");
                logger.info("orderId=" + id);
                orderService.deleteById(Long.valueOf(id));
            }
        }
        logger.info("DeleteOrderByUserCommand redirect to ShowOrdersForUserCommand for User" +
                " with userId=" + userId);
        respond.sendRedirect("/dispatcher?command=" + CommandEnum.ORDERSFORUSER.name().toLowerCase());
        return null;
    }
}
