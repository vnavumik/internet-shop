package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Feedback;
import ru.inbox.vitnau.FeedbackService;
import ru.inbox.vitnau.impl.FeedbackServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowFeedbacksCommand implements Command {
    private FeedbackService feedbackService = new FeedbackServiceImpl();
    private static final Logger logger = LogManager.getLogger(ShowFeedbacksCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long id = userPrincipal.getId();
        logger.info("Request from Admin with id=" + id + " by ShowFeedbacksCommand to show all feedbacks");
        List<Feedback> feedbackList = feedbackService.getAll();
        request.setAttribute("feedbacks", feedbackList);
        logger.info("Admin with id=" + id + " has received by ShowFeedbacksCommand following feedbacks " +
                "for his request:");

        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.FEEDBACKS_PAGE_PATH);
    }
}
