package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.User;
import ru.inbox.vitnau.UserService;
import ru.inbox.vitnau.impl.UserServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

public class ShowUsersForAdminCommand implements Command {

    private UserService userService = new UserServiceImpl();
    private static final Logger logger = LogManager.getLogger(ShowUsersForAdminCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long id = userPrincipal.getId();
        logger.info("Request from Admin with id=" + id + " by ShowUsersForAdminCommand to show all Users");
        List<User> users = userService.getAll();
        request.setAttribute("users", users);
        logger.info("Admin with id=" + id + " has received by ShowUsersForAdminCommand following Users for his " +
                "request to show all Users:");
        for (User element : users) {
            logger.info("userId=" + element.getId());
        }
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.USERS_PAGE_PATH);
    }
}
