package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.AuditService;
import ru.inbox.vitnau.ProductService;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.dto.AuditDTO;
import ru.inbox.vitnau.impl.AuditServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowAuditsForAdminCommand implements Command {

    private AuditService auditService = new AuditServiceImpl();
    private static final Logger logger = LogManager.getLogger(ShowAuditsForAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        logger.info("Request  from Admin with userId=" + userId + " by ShowAuditsForAdminCommand to show all audits");
        List<AuditDTO> auditDTOList = auditService.getAll();
        request.setAttribute("audits", auditDTOList);
        logger.info("Admin with userId=" + userId + " has received by ShowAuditsForAdminCommand following products for his " +
                "request to show all audits:");
        for (AuditDTO element : auditDTOList) {
            logger.info("auditId=" + element.getId());
        }
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.AUDITS_ADMIN_PAGE_PATH);
    }
}
