package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Feedback;

import ru.inbox.vitnau.dao.model.User;
import ru.inbox.vitnau.FeedbackService;
import ru.inbox.vitnau.UserService;
import ru.inbox.vitnau.impl.FeedbackServiceImpl;
import ru.inbox.vitnau.impl.UserServiceImpl;
import servlets.commands.Command;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AddFeedbackCommand implements Command {
    private FeedbackService feedbackService = new FeedbackServiceImpl();
    private UserService userService = new UserServiceImpl();
    private static final Logger logger = LogManager.getLogger(AddFeedbackCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {


        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        User user = userService.getById(userId);
        String message = request.getParameter("message");

        if (user != null) {
            logger.info("Request from 'userFeedback.jsp' page by AddFeedbackCommand to save " +
                    "feedback for User with userId=" + userId);
            Feedback feedback = Feedback.newBuilder().
                    withUser(user).
                    withMessage(message)
                    .build();
            feedbackService.save(feedback);
        }
        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.USER_PAGE_PATH);
    }
}
