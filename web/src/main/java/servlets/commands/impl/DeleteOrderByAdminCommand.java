package servlets.commands.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.OrderService;
import ru.inbox.vitnau.impl.OrderServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteOrderByAdminCommand implements Command {

    private OrderService orderService = new OrderServiceImpl();
    private static final Logger logger = LogManager.getLogger(DeleteOrderByAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        String[] ids = request.getParameterValues("ids");
        if (ids != null) {
            for (String id1 : ids) {
                logger.info("Request from 'orders.jsp' page from Admin with userId=" + userId +
                        " by DeleteOrderByAdminCommand to  delete  order with orderId:");
                logger.info("orderId=" + id1);
                orderService.deleteById(Long.valueOf(id1));
            }
        }
        logger.info("DeleteOrderByAdminCommand redirect to ShowOrdersForAdminCommand for Admin" +
                " with userId=" + userId);
        respond.sendRedirect("/dispatcher?command=" + CommandEnum.SHOWORDERS.name().toLowerCase());

        return null;
    }
}
