package servlets.commands.impl;

import config.ConfigurationManagerServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.ParserService;
import ru.inbox.vitnau.ProductService;
import ru.inbox.vitnau.impl.ParserServiceImpl;
import ru.inbox.vitnau.impl.ProductServiceImpl;
import servlets.commands.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

public class AddProductsByAdminCommand implements Command {

    private ProductService productService = new ProductServiceImpl();
    private ParserService parserService = ParserServiceImpl.getInstance();
    private static final Logger logger = LogManager.getLogger(AddProductsByAdminCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        File file = new File(ConfigurationManagerServlet.getInstance().
                getProperty(ConfigurationManagerServlet.FILE_PAGE_PATH));
        List<Product> productList = parserService.parser(file);
        Integer count = 0;
        for (Product element : productList) {
            logger.info("Try to add by AddProductsByAdminCommand  to the table from xml file the product with " +
                    "productUniqueNumber=" + element.getUniqueNumber());
            Product product = productService.getByUniqueNumber(element.getUniqueNumber());
            if (product == null) {
                count = count + productService.add(element);
                logger.info("It was added by  AddProductsByAdminCommand to the table  the product with " +
                        "productUniqueNumber=" + element.getUniqueNumber());
            } else {
                logger.info("This product exist in database!");
            }
        }
        request.setAttribute("number", count);
        logger.info("The number of products were added: " + count);

        return ConfigurationManagerServlet.getInstance().getProperty(ConfigurationManagerServlet.UPDATE_PAGE_PATH);
    }
}
