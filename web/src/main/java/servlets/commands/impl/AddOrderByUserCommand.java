package servlets.commands.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.model.Order;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.dao.model.User;
import ru.inbox.vitnau.OrderService;
import ru.inbox.vitnau.ProductService;
import ru.inbox.vitnau.UserService;
import ru.inbox.vitnau.impl.OrderServiceImpl;
import ru.inbox.vitnau.impl.ProductServiceImpl;
import ru.inbox.vitnau.impl.UserServiceImpl;
import servlets.commands.Command;
import servlets.model.CommandEnum;
import servlets.model.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;


public class AddOrderByUserCommand implements Command {

    private OrderService orderService = new OrderServiceImpl();
    private UserService userService = new UserServiceImpl();
    private ProductService productService = new ProductServiceImpl();
    private static final Logger logger = LogManager.getLogger(AddOrderByUserCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse respond) throws Exception {
        HttpSession session = request.getSession();
        UserPrincipal userPrincipal = (UserPrincipal) session.getAttribute("user");
        Long userId = userPrincipal.getId();
        User user = userService.getById(userId);
        String[] productIds = request.getParameterValues("ids");
        if (user != null) {
            if (productIds != null) {
                for (String idsProduct : productIds) {
                    logger.info("Request from 'productsForUser.jsp' page by AddOrderByUserCommand to save " +
                            "order for User with userId=" + userId + " and  product with productId:");
                    logger.info("productId=" + idsProduct);
                    Product product = productService.getById(Long.valueOf(idsProduct));
                    if (product != null) {
                        Order order = Order.newBuilder().
                                withProduct(product).
                                withUser(user).
                                withOrderNumber(new Date().toString())
                                .build();
                        orderService.save(order);
                    } else {
                        logger.info("Attempt to save order with product which not exist!");
                    }
                }
            }
        } else {
            logger.info("Attempt to save order with User which not exist!");
        }
        logger.info("AddOrderByUserCommand redirect to ShowProductsForUserCommand for User with userId=" + userId);
        respond.sendRedirect("/dispatcher?command=" + CommandEnum.PRODUCTSFORUSER.name().toLowerCase());
        return null;
    }
}
