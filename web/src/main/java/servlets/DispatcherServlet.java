package servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import servlets.commands.Command;
import servlets.commands.impl.*;
import servlets.model.CommandEnum;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DispatcherServlet extends HttpServlet {
    private static final Map<String, Command> commands = new HashMap<>();
    private static final Logger logger = LogManager.getLogger(DispatcherServlet.class);

    @Override
    public void init() throws ServletException {
        commands.put(CommandEnum.LOGIN.name().toLowerCase(), new LoginCommand());
        commands.put(CommandEnum.USER.name().toLowerCase(), new GoUserPageCommand());
        commands.put(CommandEnum.SHOWUSERS.name().toLowerCase(), new ShowUsersForAdminCommand());
        commands.put(CommandEnum.ADMIN.name().toLowerCase(), new GoAdminPageCommand());
        commands.put(CommandEnum.SHOWORDERS.name().toLowerCase(), new ShowOrdersForAdminCommand());
        commands.put(CommandEnum.PRODUCTSFORADMIN.name().toLowerCase(), new ShowProductsForAdminCommand());
        commands.put(CommandEnum.PRODUCTSFORUSER.name().toLowerCase(), new ShowProductsForUserCommand());
        commands.put(CommandEnum.ORDERSFORUSER.name().toLowerCase(), new ShowOrdersForUserCommand());
        commands.put(CommandEnum.ADDORDER.name().toLowerCase(), new AddOrderByUserCommand());
        commands.put(CommandEnum.DELETEORDERBYADMIN.name().toLowerCase(), new DeleteOrderByAdminCommand());
        commands.put(CommandEnum.DELETEORDERBYUSER.name().toLowerCase(), new DeleteOrderByUserCommand());
        commands.put(CommandEnum.DELETEPRODUCT.name().toLowerCase(), new DeleteProductByAdminCommand());
        commands.put(CommandEnum.DELETEUSER.name().toLowerCase(), new DeleteUserByAdminCommand());
        commands.put(CommandEnum.ADDPRODUCTS.name().toLowerCase(), new AddProductsByAdminCommand());
        commands.put(CommandEnum.ADDFEEDBACK.name().toLowerCase(), new AddFeedbackCommand());
        commands.put(CommandEnum.SHOWFEEDBACKS.name().toLowerCase(), new ShowFeedbacksCommand());
        commands.put(CommandEnum.GOFEEDBACKPAGE.name().toLowerCase(), new GoFeedbackPage());
        commands.put(CommandEnum.AUDITSFORADMIN.name().toLowerCase(), new ShowAuditsForAdminCommand());
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String command = request.getParameter("command").toLowerCase();
        Command commandAction = commands.get(command);
        if (commandAction != null) {
            try {
                String page = commandAction.execute(request, response);
                if (page != null) {
                    getServletContext().getRequestDispatcher(page).forward(request, response);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            logger.info("Command" + command + "does not exist!");
        }
    }
}
