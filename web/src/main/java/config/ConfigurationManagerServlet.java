package config;


import java.util.ResourceBundle;

public class ConfigurationManagerServlet {

    private static ConfigurationManagerServlet instance;

    private ResourceBundle resourceBundle;

    private static final String BUNDLE_NAME = "config";

    public static final String LOGIN_PAGE_PATH = "login.page.path";
    public static final String USERS_PAGE_PATH = "users.page.path";
    public static final String PRODUCTS_ADMIN_PAGE_PATH = "productsForAdmin.page.path";
    public static final String PRODUCTS_USER_PAGE_PATH = "productsForUser.page.path";
    public static final String ORDERS_PAGE_PATH = "orders.page.path";
    public static final String ORDERS_USER_PAGE_PATH = "ordersForUser.page.path";
    public static final String USER_PAGE_PATH = "user.page.path";
    public static final String ADMIN_PAGE_PATH = "admin.page.path";
    public static final String UPDATE_PAGE_PATH = "update.page.path";
    public static final String ERRORS_PAGE_PATH = "errors.page.path";
    public static final String FILE_PAGE_PATH = "file.page.path";
    public static final String USER_FEEDBACK_PAGE_PATH = "userFeedback.page.path";
    public static final String FEEDBACKS_PAGE_PATH = "feedbacks.page.path";
    public static final String AUDITS_ADMIN_PAGE_PATH = "auditsForAdmin.page.path";


    public static ConfigurationManagerServlet getInstance() {
        if (instance == null) {
            instance = new ConfigurationManagerServlet();
            instance.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
