<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Products page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form action="${pageContext.request.contextPath}/dispatcher?command=addOrder" method="post">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">ORDER</button>
                        <a href="${pageContext.request.contextPath}/dispatcher?command=user"
                           class="btn btn-primary" aria-pressed="true" role="button">USER PAGE</a>
                        <a href="${pageContext.request.contextPath}/dispatcher?command=login"
                           class="btn btn-primary" aria-pressed="true" role="button">LOGOUT</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Pizza Name</th>
                                <th scope="col">Unique Number</th>
                                <th scope="col">Description</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${products}" var="product">
                                <tr>
                                    <th scope="row"><input type="checkbox" name="ids" value="${product.id}"></th>
                                    <td>${product.name}</td>
                                    <td>${product.uniqueNumber}</td>
                                    <td>${product.description}</td>
                                    <td>${product.price}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
