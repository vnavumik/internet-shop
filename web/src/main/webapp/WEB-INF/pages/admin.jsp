<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Admin page</title>
</head>
<body>
<jsp:include page="util/logo.jsp"/>
<div class="container" align="center">
    <a href="${pageContext.request.contextPath}/dispatcher?command=showUsers"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW USERS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=productsForAdmin"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW PRODUCTS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=showOrders"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW ORDERS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=addProducts"
       class="btn btn-primary" aria-pressed="true" role="button">ADD PRODUCTS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=showFeedbacks"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW FEEDBACKS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=auditsForAdmin"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW AUDITS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=login"
       class="btn btn-primary" aria-pressed="true" role="button">LOGOUT</a>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>

