<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Login page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form action="${pageContext.request.contextPath}/dispatcher?command=addFeedback" method="post">
                <div class="form-group">
                    <label for="exampleInputPassword1">MESSAGE</label>
                    <input type="text" name="message" value="${message}" class="form-control"
                           id="exampleInputPassword1"
                           placeholder="message">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>