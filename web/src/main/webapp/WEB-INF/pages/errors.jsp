<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Errors page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <c:if test="${not empty the_status_code}">
                <div class="alert alert-danger" role="alert">
                    <c:out value="${the_status_code}"/>
                </div>
            </c:if>
            <c:if test="${not empty servlet_name}">
                <div class="alert alert-danger" role="alert">
                    <c:out value="${servlet_name}"/>
                </div>
            </c:if>
            <c:if test="${not empty exception_type}">
                <div class="alert alert-danger" role="alert">
                    <c:out value="${exception_type}"/>
                </div>
            </c:if>
            <c:if test="${not empty the_request_URI}">
                <div class="alert alert-danger" role="alert">
                    <c:out value="${the_request_URI}"/>
                </div>
            </c:if>
            <form action="${pageContext.request.contextPath}/dispatcher?command=login" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" value="${email}" class="form-control" id="exampleInputEmail1"
                           placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" value="${password}" class="form-control"
                           id="exampleInputPassword1"
                           placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<jsp:include page="util/js.jsp"/>
</body>