<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Products page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form action="${pageContext.request.contextPath}/dispatcher?command=deleteOrderByAdmin" method="post">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">DELETE</button>
                        <a href="${pageContext.request.contextPath}/dispatcher?command=admin"
                           class="btn btn-primary" aria-pressed="true" role="button">ADMIN PAGE</a>
                        <a href="${pageContext.request.contextPath}/dispatcher?command=login"
                           class="btn btn-primary" aria-pressed="true" role="button">LOGOUT</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order Number</th>
                                <th scope="col">Product</th>
                                <th scope="col">User</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${orders}" var="order">
                                <tr>
                                    <th scope="row"><input type="checkbox" name="ids" value="${order.id}"></th>
                                    <td>${order.orderNumber}</td>
                                    <td>${order.product.name}</td>
                                    <td>${order.user.firstName} ${order.user.lastName}</td>
                                    <td>${order.product.price}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">Total cost: ${total_cost}</div>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
