<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>User page</title>
</head>
<body>
<div class="container" align="center">
    <jsp:include page="util/logo.jsp"/>
    <a href="${pageContext.request.contextPath}/dispatcher?command=productsForUser"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW PRODUCTS</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=ordersForUser"
       class="btn btn-primary" aria-pressed="true" role="button">SHOW MY ORDER</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=goFeedbackpage"
       class="btn btn-primary" aria-pressed="true" role="button">ADD FEEDBACK</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=login"
       class="btn btn-primary" aria-pressed="true" role="button">LOGOUT</a>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
