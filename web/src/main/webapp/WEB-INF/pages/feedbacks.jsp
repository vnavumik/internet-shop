<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Products page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <a href="${pageContext.request.contextPath}/dispatcher?command=admin"
                       class="btn btn-primary" aria-pressed="true" role="button">ADMIN PAGE</a>
                    <a href="${pageContext.request.contextPath}/dispatcher?command=login"
                       class="btn btn-primary" aria-pressed="true" role="button">LOGOUT</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">User name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${feedbacks}" var="feedback">
                            <tr>
                              <!--  <th scope="row"><input type="checkbox" name="ids" value="${feedback.id}"></th> -->
                                <td>${feedback.id}</td>
                                <td>${feedback.user.firstName} ${feedback.user.lastName}</td>
                                <td>${feedback.user.email}</td>
                                <td>${feedback.message}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
