package ru.inbox.vitnau.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.config.ConfigurationManagerDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {

    private static ConnectionService instance;
    private Connection connection;
    private static final Logger logger = LogManager.getLogger(ConnectionService.class);

    private ConnectionService() {
        Marker marker = MarkerManager.getMarker("DAO");
        logger.info(marker, "-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName(ConfigurationManagerDao.getInstance().
                    getProperty(ConfigurationManagerDao.DATABASE_DRIVER_NAME));
            logger.info(marker, "MySQL JDBC driver is registered");
        } catch (ClassNotFoundException e) {
            logger.info(marker, "Where is your MySQL JDBC Driver?");
            logger.error(marker, e.getMessage(), e);
        }
        try {
            connection = DriverManager.
                    getConnection(ConfigurationManagerDao.getInstance().
                                    getProperty(ConfigurationManagerDao.DATABASE_URL),
                            ConfigurationManagerDao.getInstance().
                                    getProperty(ConfigurationManagerDao.DATABASE_USERNAME),
                            ConfigurationManagerDao.getInstance().
                                    getProperty(ConfigurationManagerDao.DATABASE_PWD));
            logger.info(marker, "Connection was Created");
        } catch (SQLException e) {
            logger.info(marker, "Connection Failed! Check output console");
            logger.error(marker, e.getMessage(), e);
        }
    }

    public static ConnectionService getInstance() {
        if (instance == null) {
            instance = new ConnectionService();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
