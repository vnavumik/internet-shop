package ru.inbox.vitnau.dao.model;


public class User {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String additionInfo;
    private String password;
    private Role role;

    public String getAdditionInfo() {
        return additionInfo;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", additionInfo='" + additionInfo + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role.name() +
                '}';
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    private User(Builder builder) {
        id = builder.id;
        firstName = builder.firstName;
        lastName = builder.lastName;
        email = builder.email;
        phone = builder.phone;
        additionInfo = builder.additionInfo;
        password = builder.password;
        role = builder.role;
    }

    public static final class Builder {
        private Long id;
        private String firstName;
        private String lastName;
        private String email;
        private String phone;
        private String additionInfo;
        private String password;
        private Role role;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder withLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withPhone(String val) {
            phone = val;
            return this;
        }

        public Builder withAdditionInfo(String val) {
            additionInfo = val;
            return this;
        }

        public Builder withPassword(String val) {
            password = val;
            return this;
        }

        public Builder withRole(String val) {
            if (Role.USER.name().equals(val)) {
                role = Role.USER;
            }
            if (Role.ADMIN.name().equals(val)) {
                role = Role.ADMIN;
            }
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}


