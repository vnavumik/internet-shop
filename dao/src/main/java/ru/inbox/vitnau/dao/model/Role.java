package ru.inbox.vitnau.dao.model;

public enum Role {
    USER("USER"),
    ADMIN("ADMIN");

    private final String description;

    Role(String description) {
        this.description = description;
    }
}

