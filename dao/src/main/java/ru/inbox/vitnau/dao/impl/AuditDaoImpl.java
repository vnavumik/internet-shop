package ru.inbox.vitnau.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.dao.AuditDao;
import ru.inbox.vitnau.dao.model.Audit;

public class AuditDaoImpl extends GenericDaoImpl<Audit> implements AuditDao {

    private static final Logger logger = LogManager.getLogger(AuditDaoImpl.class);
    private Marker marker = MarkerManager.getMarker("DAO");

    public AuditDaoImpl(Class<Audit> clazz) {
        super(clazz);
    }
}
