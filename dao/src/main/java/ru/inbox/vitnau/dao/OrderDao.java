package ru.inbox.vitnau.dao;

import ru.inbox.vitnau.dao.model.Order;

import java.sql.Connection;
import java.util.List;

public interface OrderDao {

    List<Order> getAll(Connection connection);

    List<Order> getByUserId(Connection connection, Long id);

    void deleteById(Connection connection, Long id);

    void save(Connection connection, Order order);

}
