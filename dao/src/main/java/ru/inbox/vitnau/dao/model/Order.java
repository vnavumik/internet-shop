package ru.inbox.vitnau.dao.model;


public class Order {
    private Long id;
    private String orderNumber;
    private User user;
    private Product product;

    private Order(Builder builder) {
        id = builder.id;
        orderNumber = builder.orderNumber;
        user = builder.user;
        product = builder.product;
    }

    public Long getId() {
        return id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public User getUser() {
        return user;
    }

    public Product getProduct() {
        return product;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNumber=" + orderNumber +
                ", user=" + user +
                ", product=" + product +
                '}';
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private String orderNumber;
        private User user;
        private Product product;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withOrderNumber(String val) {
            orderNumber = val;
            return this;
        }

        public Builder withUser(User val) {
            user = val;
            return this;
        }

        public Builder withProduct(Product val) {
            product = val;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }
}
