package ru.inbox.vitnau.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.dao.ProductDao;
import ru.inbox.vitnau.dao.model.Product;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {
    private static final Logger logger = LogManager.getLogger(ProductDaoImpl.class);
    private Marker marker = MarkerManager.getMarker("DAO");

    @Override
    public void add(Connection connection, Product product) {
        Long uniqueNumber = product.getUniqueNumber();
        logger.info(marker, "Method in  ProductDaoImpl try to add  to the table the product with uniqueNumber=" +
                uniqueNumber);
        String addToTheTableSQL = "INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES (?,?,?,?)";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(addToTheTableSQL)) {
                preparedStatement.setString(1, product.getName());
                preparedStatement.setLong(2, product.getUniqueNumber());
                preparedStatement.setString(3, product.getDescription());
                preparedStatement.setBigDecimal(4, product.getPrice());
                preparedStatement.executeUpdate();
                logger.info(marker, "It was added by ProductDaoImpl to the table the product with uniqueNumber=" +
                        uniqueNumber);
            } catch (SQLException e) {
                logger.info(marker, "Method in ProductDaoImpl can't add to the table the product with uniqueNumber=" +
                        uniqueNumber);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }

    @Override
    public List<Product> getAll(Connection connection) {
        logger.info(marker, "Method in  ProductDaoImpl try to get all products");
        List<Product> productList = new ArrayList<>();
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE FROM T_PRODUCT";
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                resultSet = statement.executeQuery(getFromTheTableSQL);
                while (resultSet.next()) {
                    try {
                        Product product = getProduct(resultSet);
                        productList.add(product);
                    } catch (SQLException e) {
                        logger.info(marker, "Can't get by ProductDaoImpl  all products");
                        logger.error(marker, e.getMessage(), e);
                    }
                }
                logger.info(marker, "Return by ProductDaoImpl following products with productId:");
                for (Product element : productList) {
                    logger.info(marker, "productId=" + element.getId());
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by ProductDaoImpl  all products");
                logger.error(marker, e.getMessage(), e);
            }
        }
        return productList;
    }

    @Override
    public Product getById(Connection connection, Long productId) {
        logger.info(marker, "Try to get by ProductDaoImpl the product with productId=" + productId);
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE FROM T_PRODUCT WHERE ID=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, productId);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    logger.info(marker, "Return  by ProductDaoImpl the product with productId=" + productId);
                    return getProduct(resultSet);
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by ProductDaoImpl the product with productId=" + productId);
                logger.error(marker, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public Product getByUniqueNumber(Connection connection, Long uniqueNumber) {
        logger.info(marker, "Try to get by ProductDaoImpl the product with uniqueNumber=" + uniqueNumber);
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE FROM T_PRODUCT WHERE UNIQUE_NUMBER=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, uniqueNumber);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    logger.info(marker, "Return  by ProductDaoImpl the product with uniqueNumber=" + uniqueNumber);
                    return getProduct(resultSet);
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by ProductDaoImpl the product with uniqueNumber=" + uniqueNumber);
                logger.error(marker, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public void deleteById(Connection connection, Long productId) {
        logger.info(marker, "Try to delete by ProductDaoImpl the product with productId=" + productId);
        String getFromTheTableSQL = "DELETE FROM T_PRODUCT WHERE ID=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, productId);
                preparedStatement.executeUpdate();
                logger.info(marker, "It was deleted by ProductDaoImpl the product with productId=" + productId);
            } catch (SQLException e) {
                logger.info(marker, "Can't delete by ProductDaoImpl the product with productId=" + productId);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }

    private Product getProduct(ResultSet resultSet) throws SQLException {

        return Product.newBuilder().
                withId(resultSet.getLong("ID")).
                withName(resultSet.getString("NAME")).
                withUniqueNumber(resultSet.getLong("UNIQUE_NUMBER")).
                withDescription(resultSet.getString("DESCRIPTION")).
                withPrice(resultSet.getBigDecimal("PRICE")).
                build();
    }
}
