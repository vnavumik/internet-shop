package ru.inbox.vitnau.dao;

import ru.inbox.vitnau.dao.model.User;

import java.sql.Connection;
import java.util.List;

public interface UserDao {

    User getByEmail(Connection connection, String email);

    List<User> getAll(Connection connection);

    User getById(Connection connection, Long userId);

    void deleteById(Connection connection, Long userId);

}