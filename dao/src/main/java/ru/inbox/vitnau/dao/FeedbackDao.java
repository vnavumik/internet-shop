package ru.inbox.vitnau.dao;

import ru.inbox.vitnau.dao.model.Feedback;

import java.sql.Connection;
import java.util.List;

public interface FeedbackDao {

    void save(Connection connection, Feedback feedback);

    List<Feedback> getAll (Connection connection);

}
