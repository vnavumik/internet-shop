package ru.inbox.vitnau.dao.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name="T_AUDIT")
public class Audit implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "ID",updatable = false,nullable = false)
    private Long id;
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "EVENT_TYPE")
    private String eventType;
    @Column(name = "CREATED")
    private LocalDateTime created;

    private Audit(Builder builder) {
        id = builder.id;
        userId = builder.userId;
        eventType = builder.eventType;
        created = builder.created;
    }

    public Audit() {
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public String getEventType() {
        return eventType;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "Audit{" +
                "id=" + id +
                ", userId=" + userId +
                ", eventType='" + eventType + '\'' +
                ", created=" + created +
                '}';
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private Long userId;
        private String eventType;
        private LocalDateTime created;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withUserId(Long val) {
            userId = val;
            return this;
        }

        public Builder withEventType(String val) {
            eventType = val;
            return this;
        }

        public Builder withCreated(LocalDateTime val) {
            created = val;
            return this;
        }

        public Audit build() {
            return new Audit(this);
        }
    }
}
