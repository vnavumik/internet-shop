package ru.inbox.vitnau.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.dao.OrderDao;
import ru.inbox.vitnau.dao.model.Order;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.dao.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {
    private static final Logger logger = LogManager.getLogger(OrderDaoImpl.class);
    private Marker marker = MarkerManager.getMarker("DAO");

    @Override
    public List<Order> getAll(Connection connection) {
        List<Order> ordersList = new ArrayList<>();
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ORD.ID AS ORDER_ID,ORDER_NUMBER,\n" +
                "PRODUCT_ID,NAME ,UNIQUE_NUMBER,DESCRIPTION,PRICE,\n" +
                "USER_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD\n" +
                " FROM (T_ORDER ORD INNER JOIN T_PRODUCT PR ON PRODUCT_ID=PR.ID) INNER JOIN T_USER US" +
                " ON USER_ID=US.ID";
        logger.info(marker, "Try to get by OrderDaoImpl all orders");
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                resultSet = statement.executeQuery(getFromTheTableSQL);
                while (resultSet.next()) {
                    try {
                        Product product = Product.newBuilder().
                                withId(resultSet.getLong("PRODUCT_ID")).
                                withName(resultSet.getString("NAME")).
                                withUniqueNumber(resultSet.getLong("UNIQUE_NUMBER")).
                                withDescription(resultSet.getString("DESCRIPTION")).
                                withPrice(resultSet.getBigDecimal("PRICE")).
                                build();
                        User user = User.newBuilder().
                                withId(resultSet.getLong("USER_ID")).
                                withFirstName(resultSet.getString("FIRST_NAME")).
                                withLastName(resultSet.getString("LAST_NAME")).
                                withEmail(resultSet.getString("EMAIL")).
                                withPhone(resultSet.getString("PHONE")).
                                withPassword(resultSet.getString("PASSWORD")).
                                withAdditionInfo(resultSet.getString("ADDITION_INFO")).
                                withRole(resultSet.getString("ROLE"))
                                .build();
                        Order order = Order.newBuilder().
                                withId(resultSet.getLong("ORDER_ID")).
                                withOrderNumber(resultSet.getString("ORDER_NUMBER")).
                                withUser(user).
                                withProduct(product).
                                build();
                        ordersList.add(order);
                    } catch (SQLException e) {
                        logger.info(marker, "Can't get by OrderDaoImpl  all orders");
                        logger.error(marker, e.getMessage(), e);
                    }
                }
                logger.info(marker, "Return by OrderDaoImpl following orders with orderId:");
                for (Order element : ordersList) {
                    logger.info(marker, "orderId=" + element.getId());
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by OrderDaoImpl all orders");
                logger.error(marker, e.getMessage(), e);
            }
        }
        return ordersList;
    }

    @Override
    public List<Order> getByUserId(Connection connection, Long userId) {
        List<Order> ordersList = new ArrayList<>();
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ORD.ID AS ORDER_ID,ORDER_NUMBER,\n" +
                "PRODUCT_ID,NAME ,UNIQUE_NUMBER,DESCRIPTION,PRICE,\n" +
                "USER_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD\n" +
                " FROM (T_ORDER ORD INNER JOIN T_PRODUCT PR ON PRODUCT_ID=PR.ID) INNER JOIN T_USER US" +
                " ON USER_ID=US.ID WHERE USER_ID=?";
        logger.info(marker, "Try to get by OrderDaoImpl all orders for User with userId=" + userId);
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, userId);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    try {
                        Product product = Product.newBuilder().
                                withId(resultSet.getLong("PRODUCT_ID")).
                                withName(resultSet.getString("NAME")).
                                withUniqueNumber(resultSet.getLong("UNIQUE_NUMBER")).
                                withDescription(resultSet.getString("DESCRIPTION")).
                                withPrice(resultSet.getBigDecimal("PRICE")).
                                build();
                        User user = User.newBuilder().
                                withId(resultSet.getLong("USER_ID")).
                                withFirstName(resultSet.getString("FIRST_NAME")).
                                withLastName(resultSet.getString("LAST_NAME")).
                                withEmail(resultSet.getString("EMAIL")).
                                withPhone(resultSet.getString("PHONE")).
                                withPassword(resultSet.getString("PASSWORD")).
                                withAdditionInfo(resultSet.getString("ADDITION_INFO")).
                                withRole(resultSet.getString("ROLE"))
                                .build();
                        Order order = Order.newBuilder().
                                withId(resultSet.getLong("ORDER_ID")).
                                withOrderNumber(resultSet.getString("ORDER_NUMBER")).
                                withUser(user).
                                withProduct(product).
                                build();
                        ordersList.add(order);
                    } catch (SQLException e) {
                        logger.info(marker, "Can't get by OrderDaoImpl all orders for user with id=" + userId);
                        logger.error(marker, e.getMessage(), e);
                    }
                }
                logger.info(marker, "Return by OrderDaoImpl for User with userId=" + userId + " following orders with orderId:");
                for (Order element : ordersList) {
                    logger.info(marker, "orderId=" + element.getId());
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by OrderDaoImpl all orders  for User with userId=" + userId);
                logger.error(marker, e.getMessage(), e);
            }
        }
        return ordersList;
    }


    @Override
    public void deleteById(Connection connection, Long id) {
        String deleteFromTheTableSQL = "DELETE FROM T_ORDER WHERE ID=?";
        logger.info(marker, "Try to delete by OrderDaoImpl order with orderId=" + id);
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(deleteFromTheTableSQL)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                logger.info(marker, "It was deleted by OrderDaoImpl order with orderId=" + id);
            } catch (SQLException e) {
                logger.info(marker, "Can't delete by OrderDaoImpl  order with orderId=" + id);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }

    @Override
    public void save(Connection connection, Order order) {
        Long userId = order.getUser().getId();
        logger.info(marker, "Try to save by OrderDaoImpl order for User with userId=:" + userId);
        String saveIntoTableSQL = "INSERT INTO T_ORDER (ORDER_NUMBER,USER_ID,PRODUCT_ID) VALUES (?,?,?)";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(saveIntoTableSQL)) {
                preparedStatement.setString(1, order.getOrderNumber());
                preparedStatement.setLong(2, userId);
                preparedStatement.setLong(3, order.getProduct().getId());
                preparedStatement.executeUpdate();
                logger.info(marker, "Order was saved  by OrderDaoImpl for User with userId=" + userId);
            } catch (SQLException e) {
                logger.info(marker, "Order wasn't saved  by OrderDaoImpl for User with userId=" + userId);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }
}
