package ru.inbox.vitnau.dao.model;

public class Feedback {

    private Long id;
    private User user;
    private String message;

    private Feedback(Builder builder) {
        setId(builder.id);
        setUser(builder.user);
        setMessage(builder.message);
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", user=" + user +
                ", message='" + message + '\'' +
                '}';
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private User user;
        private String message;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withUser(User val) {
            user = val;
            return this;
        }

        public Builder withMessage(String val) {
            message = val;
            return this;
        }

        public Feedback build() {
            return new Feedback(this);
        }
    }
}
