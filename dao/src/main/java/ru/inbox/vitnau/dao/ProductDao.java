package ru.inbox.vitnau.dao;

import ru.inbox.vitnau.dao.model.Product;

import java.sql.Connection;
import java.util.List;

public interface ProductDao {

    void add(Connection connection, Product product);

    List<Product> getAll(Connection connection);

    Product getById(Connection connection, Long productId);

    Product getByUniqueNumber(Connection connection, Long uniqueNumber);

    void deleteById(Connection connection, Long productId);

}
