package ru.inbox.vitnau.dao;

import ru.inbox.vitnau.dao.model.Audit;

public interface AuditDao extends GenericDao<Audit> {
}
