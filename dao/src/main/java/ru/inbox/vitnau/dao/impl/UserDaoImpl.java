package ru.inbox.vitnau.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.dao.UserDao;
import ru.inbox.vitnau.dao.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);
    private Marker marker = MarkerManager.getMarker("DAO");

    @Override
    public User getByEmail(Connection connection, String email) {
        logger.info(marker, "Try to get by UserDaoImpl the User with email=" + email);
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD FROM T_USER WHERE EMAIL=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setString(1, email);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    logger.info(marker, "Return  by UserDaoImpl the User with email=" + email);
                    return getUser(resultSet);
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by UserDaoImpl the User with email=" + email);
                logger.error(marker, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public List<User> getAll(Connection connection) {
        logger.info(marker, "Try to get by UserDaoImpl all Users");
        List<User> userList = new ArrayList<>();
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD FROM T_USER";
        if (connection != null) {
            try (Statement statement = connection.createStatement()
            ) {
                resultSet = statement.executeQuery(getFromTheTableSQL);
                while (resultSet.next()) {
                    try {
                        User user = getUser(resultSet);
                        userList.add(user);
                    } catch (SQLException e) {
                        logger.info(marker, "Can't get by UserDaoImpl all Users");
                        logger.error(marker, e.getMessage(), e);
                    }
                }
                logger.info(marker, "Return by UserDaoImpl following Users with userId:");
                for (User element : userList) {
                    logger.info(marker, "userId=" + element.getId());
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by UserDaoImpl all Users");
                logger.error(marker, e.getMessage(), e);
            }
        }
        return userList;
    }

    @Override
    public User getById(Connection connection, Long userId) {
        logger.info(marker, "Try to get by UserDaoImpl the User with userId=" + userId);
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD FROM T_USER WHERE ID=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, userId);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    logger.info(marker, "Return  by UserDaoImpl the User with userId=" + userId);
                    return getUser(resultSet);
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by UserDaoImpl the User with userId=" + userId);
                logger.error(marker, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public void deleteById(Connection connection, Long userId) {
        logger.info(marker, "Try to delete by UserDaoImpl the User with userId=" + userId);
        String getFromTheTableSQL = "DELETE FROM T_USER WHERE ID=?";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFromTheTableSQL)) {
                preparedStatement.setLong(1, userId);
                preparedStatement.executeUpdate();
                logger.info(marker, "It was deleted by  by UserDaoImpl the User with userId=" + userId);
            } catch (SQLException e) {
                logger.info(marker, "Can't delete by by UserDaoImpl the User with userId=" + userId);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }


    private User getUser(ResultSet resultSet) throws SQLException {

        return User.newBuilder().
                withId(resultSet.getLong("ID")).
                withFirstName(resultSet.getString("FIRST_NAME")).
                withLastName(resultSet.getString("LAST_NAME")).
                withEmail(resultSet.getString("EMAIL")).
                withPhone(resultSet.getString("PHONE")).
                withPassword(resultSet.getString("PASSWORD")).
                withAdditionInfo(resultSet.getString("ADDITION_INFO")).
                withRole(resultSet.getString("ROLE"))
                .build();
    }
}

