package ru.inbox.vitnau.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import ru.inbox.vitnau.dao.FeedbackDao;
import ru.inbox.vitnau.dao.model.Feedback;
import ru.inbox.vitnau.dao.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDaoImpl implements FeedbackDao {
    private static final Logger logger = LogManager.getLogger(FeedbackDaoImpl.class);
    private Marker marker = MarkerManager.getMarker("DAO");

    @Override
    public void save(Connection connection, Feedback feedback) {
        Long userId = feedback.getUser().getId();
        logger.info(marker, "Try to save by FeedbackDaoImpl feedback for User with userId=:" + userId);
        String saveIntoTableSQL = "INSERT INTO T_FEEDBACK (USER_ID,MESSAGE) VALUES (?,?)";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(saveIntoTableSQL)) {
                preparedStatement.setLong(1, userId);
                preparedStatement.setString(2, feedback.getMessage());
                preparedStatement.executeUpdate();
                logger.info(marker, "feedback was saved  by FeedbackDaoImpl for User with userId=" + userId);
            } catch (SQLException e) {
                logger.info(marker, "feedback wasn't saved  by FeedbackDaoImpl for User with userId=" + userId);
                logger.error(marker, e.getMessage(), e);
            }
        }
    }

    @Override
    public List<Feedback> getAll(Connection connection) {
        List<Feedback> feedbackList = new ArrayList<>();
        ResultSet resultSet;
        String getFromTheTableSQL = "SELECT F.ID AS FEEDBACK_ID,USER_ID,MESSAGE,\n" +
                "FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDITION_INFO,ROLE,PASSWORD\n" +
                " FROM T_FEEDBACK F INNER JOIN T_USER US ON USER_ID=US.ID";
        logger.info(marker, "Try to get by FeedbackDaoImpl all feedbacks");
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                resultSet = statement.executeQuery(getFromTheTableSQL);
                while (resultSet.next()) {
                    try {
                        User user = User.newBuilder().
                                withId(resultSet.getLong("USER_ID")).
                                withFirstName(resultSet.getString("FIRST_NAME")).
                                withLastName(resultSet.getString("LAST_NAME")).
                                withEmail(resultSet.getString("EMAIL")).
                                withPhone(resultSet.getString("PHONE")).
                                withPassword(resultSet.getString("PASSWORD")).
                                withAdditionInfo(resultSet.getString("ADDITION_INFO")).
                                withRole(resultSet.getString("ROLE"))
                                .build();
                        Feedback feedback = Feedback.newBuilder().
                                withId(resultSet.getLong("FEEDBACK_ID")).
                                withUser(user).
                                withMessage(resultSet.getString("MESSAGE")).
                                build();
                        feedbackList.add(feedback);
                    } catch (SQLException e) {
                        logger.info(marker, "Can't get by FeedbackDaoImpl  all feedbacks");
                        logger.error(marker, e.getMessage(), e);
                    }
                }
                logger.info(marker, "Return by FeedbackDaoImpl following feedbacks with feedbackId:");
                for (Feedback element : feedbackList) {
                    logger.info(marker, "feedbackId=" + element.getId());
                }
            } catch (SQLException e) {
                logger.info(marker, "Can't get by FeedbackDaoImpl all feedbacks");
                logger.error(marker, e.getMessage(), e);
            }
        }
        return feedbackList;
    }
}
