package ru.inbox.vitnau.dao.model;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement(name = "product")
public class Product {

    private Long id;
    private String name;
    private Long uniqueNumber;
    private String description;
    private BigDecimal price;

    private Product(Builder builder) {
        id = builder.id;
        name = builder.name;
        uniqueNumber = builder.uniqueNumber;
        description = builder.description;
        price = builder.price;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    @XmlAttribute
    public void setUniqueNumber(Long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    @XmlElement(name = "description")
    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "price")
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getUniqueNumber() {
        return uniqueNumber;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uniqueNumber=" + uniqueNumber +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private String name;
        private Long uniqueNumber;
        private String description;
        private BigDecimal price;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withUniqueNumber(Long val) {
            uniqueNumber = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withPrice(BigDecimal val) {
            price = val;
            return this;
        }

        public Product build() {
            return new Product(this);
        }
    }
}
