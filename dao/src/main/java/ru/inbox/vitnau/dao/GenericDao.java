package ru.inbox.vitnau.dao;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T extends Serializable> {

    T getById(final long entityId);

    List<T> getAll();

    void create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);

    Session getCurrentSession();

}
