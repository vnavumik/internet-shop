package ru.inbox.vitnau.dao.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.inbox.vitnau.config.ConfigurationManagerDao;
import ru.inbox.vitnau.dao.model.Audit;
import ru.inbox.vitnau.dao.model.User;

import java.util.HashMap;
import java.util.Map;


public class HibernateUtil {
    private static final Logger logger = LogManager.getLogger(HibernateUtil.class);
    private static Marker marker = MarkerManager.getMarker("DAO");

    private static StandardServiceRegistry registry;
    private static SessionFactory sessionFactory;

    private HibernateUtil() {

    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
                Map<String, String> settings = new HashMap<>();
                settings.put(Environment.DRIVER, ConfigurationManagerDao.getInstance().
                        getProperty(ConfigurationManagerDao.DATABASE_DRIVER_NAME));
                settings.put(Environment.URL, ConfigurationManagerDao.getInstance().
                        getProperty(ConfigurationManagerDao.DATABASE_URL));
                settings.put(Environment.USER, ConfigurationManagerDao.getInstance().
                        getProperty(ConfigurationManagerDao.DATABASE_USERNAME));
                settings.put(Environment.PASS, ConfigurationManagerDao.getInstance().
                        getProperty(ConfigurationManagerDao.DATABASE_PWD));
                settings.put(Environment.HBM2DDL_AUTO, ConfigurationManagerDao.getInstance().
                        getProperty(Environment.HBM2DDL_AUTO));
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, ConfigurationManagerDao.getInstance().
                        getProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS));
                registryBuilder.applySettings(settings);
                registry = registryBuilder.build();
                logger.info(marker, "Hibernate Registry  builder created.");

                MetadataSources sources = new MetadataSources(registry).
                      //  addAnnotatedClass(User.class).
                        addAnnotatedClass(Audit.class);

                Metadata metadata = sources.getMetadataBuilder().build();
                sessionFactory = metadata.getSessionFactoryBuilder().build();
                logger.info(marker, "SessionFactory created.");

            } catch (Exception e) {
                logger.error(marker, "SessionFactory creation failed.");
                logger.error(marker,e.getMessage(),e);
                if (registry!=null){
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return sessionFactory;
    }
}
