CREATE TABLE IF NOT EXISTS T_USER(
                ID INTEGER (5) AUTO_INCREMENT NOT NULL,
                FIRST_NAME VARCHAR (20) NOT NULL,
                LAST_NAME VARCHAR (20) NOT NULL,
                EMAIL VARCHAR (20) NOT NULL,
                PHONE VARCHAR (20) NOT NULL,
                ADDITION_INFO TEXT,
                ROLE VARCHAR (20) NOT NULL,
                PASSWORD VARCHAR (20) NOT NULL,
                PRIMARY KEY (ID));

INSERT INTO T_USER (FIRST_NAME,LAST_NAME,EMAIL,PHONE,ROLE,PASSWORD) VALUES ('Vitali','Navumik','admin@admin','80296623326','ADMIN','admin');
INSERT INTO T_USER (FIRST_NAME,LAST_NAME,EMAIL,PHONE,ROLE,PASSWORD) VALUES ('Ivan','Ivanov','user@user','80291234567','USER','user');

CREATE TABLE IF NOT EXISTS T_PRODUCT(
                ID INTEGER (5) AUTO_INCREMENT NOT NULL,
                NAME VARCHAR (20) NOT NULL,
                UNIQUE_NUMBER INTEGER (5) NOT NULL,
                DESCRIPTION TEXT (255) NOT NULL,
                PRICE FLOAT NOT NULL,
                PRIMARY KEY (ID));
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Pepperoni',10,'pizza sauce, pepperoni, mozzarella cheese, basil',18.50);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Italian',11,'pizza sauce, pepperoni, fresh champignons, brisket (pork), olives, mozzarella cheese, basil',18.50);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Hawaiian',12,'cheese sauce, ham, chicken fillet, pineapple, mozzarella cheese, basil',15.00);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Mushroom',13,'Garlic sauce, ham, fresh champignons, mozzarella cheese, basil',15.50);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Mexican',14,'pizza sauce, chicken fillet, fresh tomatoes, fresh champignons, fresh Bulgarian pepper, fresh onions, jalapeno pepper, mozzarella cheese, basil',18.50);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Four seasons',15,'garlic sauce, pepperoni, ham, fresh champignons, chicken fillet, mozzarella cheese, basil',18.50);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Bavarian',16,'sweet mustard sauce, hunting sausages, fresh champignons, fresh onions, fresh tomatoes, mozzarella cheese, basil',15.00);
INSERT INTO T_PRODUCT (NAME,UNIQUE_NUMBER,DESCRIPTION,PRICE) VALUES ('Ukrainian',17,'garlic sauce, brisket (pork), ham, fresh onions, pickles, sweet mustard sauce, mozzarella cheese, basil',20.50);

CREATE TABLE IF NOT EXISTS T_ORDER(
                ID INTEGER (5) AUTO_INCREMENT NOT NULL,
                ORDER_NUMBER TEXT NOT NULL,
                USER_ID INTEGER (5) NOT NULL REFERENCES T_USER(ID),
                PRODUCT_ID INTEGER (5) NOT NULL REFERENCES T_PRODUCT(ID),
                PRIMARY KEY (ID));
CREATE TABLE IF NOT EXISTS T_FEEDBACK(
                ID INTEGER (5) AUTO_INCREMENT NOT NULL,
			          USER_ID INTEGER (5) NOT NULL REFERENCES T_USER(ID),
                MESSAGE TEXT NOT NULL,
                PRIMARY KEY (ID));
