package ru.inbox.vitnau;

import ru.inbox.vitnau.dao.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getByEmail(String email);

    User getById(Long userId);

    void deleteById(Long userId);
}
