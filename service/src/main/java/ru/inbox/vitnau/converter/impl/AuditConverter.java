package ru.inbox.vitnau.converter.impl;

import ru.inbox.vitnau.converter.Converter;
import ru.inbox.vitnau.dao.model.Audit;
import ru.inbox.vitnau.dto.AuditDTO;

import java.util.List;
import java.util.stream.Collectors;

public class AuditConverter implements Converter<AuditDTO, Audit> {
    @Override
    public Audit toEntity(AuditDTO dto) {
        return Audit.newBuilder()
                .withId(dto.getId())
                .withUserId(dto.getUserId())
                .withEventType(dto.getEventType())
                .withCreated(dto.getCreated())
                .build();
    }

    @Override
    public List<Audit> toEntityList(List<AuditDTO> list) {
        return list.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
