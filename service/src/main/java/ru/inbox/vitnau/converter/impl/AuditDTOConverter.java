package ru.inbox.vitnau.converter.impl;

import ru.inbox.vitnau.converter.DTOConverter;
import ru.inbox.vitnau.dao.model.Audit;
import ru.inbox.vitnau.dto.AuditDTO;

import java.util.List;
import java.util.stream.Collectors;

public class AuditDTOConverter implements DTOConverter<AuditDTO, Audit> {
    @Override
    public AuditDTO toDTO(Audit entity) {
        AuditDTO auditDTO = new AuditDTO();
        auditDTO.setId(entity.getId());
        auditDTO.setUserId(entity.getUserId());
        auditDTO.setEventType(entity.getEventType());
        auditDTO.setCreated(entity.getCreated());
        return auditDTO;
    }

    @Override
    public List<AuditDTO> toDTOList(List<Audit> list) {
        return list.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
