package ru.inbox.vitnau.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.ProductDao;
import ru.inbox.vitnau.dao.connection.ConnectionService;
import ru.inbox.vitnau.dao.impl.ProductDaoImpl;
import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.ProductService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private static final Logger logger = LogManager.getLogger(ProductServiceImpl.class);
    private ProductDao productDao = new ProductDaoImpl();

    @Override
    public Integer add(Product product) {
        Long uniqueNumber = product.getUniqueNumber();
        logger.info("Try to add by ProductServiceImpl the product with uniqueNumber= " + uniqueNumber);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            productDao.add(connection, product);
            connection.commit();
            logger.info("It was added  by ProductServiceImpl the product with uniqueNumber=" + uniqueNumber);
            return 1;
        } catch (SQLException e) {
            logger.info("Can't add by ProductServiceImpl the product with uniqueNumber=" + uniqueNumber);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'add' of ProductServiceImpl" +
                    " for product with uniqueNumber=" + uniqueNumber);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'add' of ProductServiceImpl " +
                        "for product with uniqueNumber=" + uniqueNumber);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'add' of ProductServiceImpl " +
                        "for product with uniqueNumber=" + uniqueNumber);
                logger.error(ex.getMessage(), ex);
            }
        }
        return 0;
    }


    @Override
    public List<Product> getAll() {
        logger.info("Try to get by ProductServiceImpl all products");
        List<Product> productList = new ArrayList<>();
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            productList = productDao.getAll(connection);
            connection.commit();
            logger.info("Return by ProductServiceImpl following products with productId:");
            for (Product element : productList) {
                logger.info("productId=" + element.getId());
            }
        } catch (SQLException e) {
            logger.info("Can't get by ProductServiceImpl all products");
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback  in the method 'getAll' of ProductServiceImpl");
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getAll' of ProductServiceImpl");
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getAll' of ProductServiceImpl");
                logger.error(ex.getMessage(), ex);
            }
        }
        return productList;
    }

    @Override
    public Product getById(Long productId) {
        logger.info("Try to get by ProductServiceImpl the product with productId=" + productId);
        Connection connection = ConnectionService.getInstance().getConnection();
        Product product = Product.newBuilder().build();
        try {
            connection.setAutoCommit(false);
            product = productDao.getById(connection, productId);
            connection.commit();
            logger.info("Return by ProductServiceImpl the product with productId=" + productId);
        } catch (SQLException e) {
            logger.info("Can't get by ProductServiceImpl the product with productId=" + productId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'getById' of ProductServiceImpl" +
                    " for product with productId=" + productId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getById' of ProductServiceImpl" +
                        " for product with productId=" + productId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getById' of ProductServiceImpl" +
                        " for product with productId=" + productId);
                logger.error(ex.getMessage(), ex);
            }
        }
        return product;
    }

    @Override
    public Product getByUniqueNumber(Long uniqueNumber) {
        logger.info("Try to get by ProductServiceImpl the product with uniqueNumber=" + uniqueNumber);
        Connection connection = ConnectionService.getInstance().getConnection();
        Product product = Product.newBuilder().build();
        try {
            connection.setAutoCommit(false);
            product = productDao.getByUniqueNumber(connection, uniqueNumber);
            connection.commit();
            if (product != null) {
                logger.info("Return by ProductServiceImpl the product with uniqueNumber=" + uniqueNumber);
            } else {
                logger.info("It's not exist in database the product with uniqueNumber=" + uniqueNumber);
            }
        } catch (SQLException e) {
            logger.info("Can't get by ProductServiceImpl the product with uniqueNumber=" + uniqueNumber);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'getByUniqueNumber' of ProductServiceImpl" +
                    " for product with uniqueNumber=" + uniqueNumber);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getByUniqueNumber' of ProductServiceImpl" +
                        " for product with uniqueNumber=" + uniqueNumber);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getByUniqueNumber' of ProductServiceImpl" +
                        " for product with uniqueNumber=" + uniqueNumber);
                logger.error(ex.getMessage(), ex);
            }
        }
        return product;
    }

    @Override
    public void deleteById(Long productId) {
        logger.info("Try to delete by ProductServiceImpl the product with productId=" + productId);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            productDao.deleteById(connection, productId);
            connection.commit();
            logger.info("It was deleted by ProductServiceImpl the product with productId=" + productId);
        } catch (SQLException e) {
            logger.info("Can't delete by ProductServiceImpl the product with productId=" + productId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'delete' of ProductServiceImpl" +
                    " for product with productId=" + productId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'delete' of ProductServiceImpl" +
                        " for product with productId=" + productId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'delete' of ProductServiceImpl" +
                        " for product with productId=" + productId);
                logger.error(ex.getMessage(), ex);
            }
        }
    }
}
