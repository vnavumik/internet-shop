package ru.inbox.vitnau.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.UserDao;
import ru.inbox.vitnau.dao.connection.ConnectionService;
import ru.inbox.vitnau.dao.impl.UserDaoImpl;
import ru.inbox.vitnau.dao.model.User;
import ru.inbox.vitnau.UserService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
    private UserDao userDao = new UserDaoImpl();

    @Override
    public List<User> getAll() {
        logger.info("Try to get by UserServiceImpl all Users");
        List<User> userList = new ArrayList<>();
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            userList = userDao.getAll(connection);
            connection.commit();
            logger.info("Return by UserServiceImpl following Users with userId:");
            for (User element : userList) {
                logger.info("userId=" + element.getId());
            }
        } catch (SQLException e) {
            logger.info("Can't get by UserServiceImpl all Users");
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback  in the method 'getAll' of UserServiceImpl");
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getAll' of UserServiceImpl");
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getAll' of UserServiceImpl");
                logger.error(ex.getMessage(), ex);
            }
        }
        return userList;
    }

    @Override
    public User getByEmail(String email) {
        logger.info("Try to get by UserServiceImpl the User with email=" + email);
        Connection connection = ConnectionService.getInstance().getConnection();
        User user = User.newBuilder().build();
        try {
            connection.setAutoCommit(false);
            user = userDao.getByEmail(connection, email);
            connection.commit();
            logger.info("Return by UserServiceImpl the  User with email=" + email);
        } catch (SQLException e) {
            logger.info("Can't get by UserServiceImpl the  User with email=" + email);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'getByEmail' of UserServiceImpl" +
                    " for  User with email=" + email);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getByEmail' of UserServiceImpl" +
                        " for  User with email=" + email);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getByEmail' of UserServiceImpl" +
                        " for  User with email=" + email);
                logger.error(ex.getMessage(), ex);
            }
        }
        return user;
    }

    @Override
    public User getById(Long userId) {
        logger.info("Try to get by UserServiceImpl the User with userId=" + userId);
        Connection connection = ConnectionService.getInstance().getConnection();
        User user = User.newBuilder().build();
        try {
            connection.setAutoCommit(false);
            user = userDao.getById(connection, userId);
            connection.commit();
            logger.info("Return by UserServiceImpl the  User with userId=" + userId);
        } catch (SQLException e) {
            logger.info("Can't get by UserServiceImpl the  User with userId=" + userId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'getById' of UserServiceImpl" +
                    " for  User with User with userId=" + userId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getById' of UserServiceImpl" +
                        " for  User with User with userId=" + userId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getById' of UserServiceImpl" +
                        " for  User with User with userId=" + userId);
                logger.error(ex.getMessage(), ex);
            }
        }
        return user;
    }

    @Override
    public void deleteById(Long userId) {
        logger.info("Try to delete user=" + userId);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            userDao.deleteById(connection, userId);
            connection.commit();
            logger.info("It was deleted by UserServiceImpl the User with userId=" + userId);
        } catch (SQLException e) {
            logger.info("Can't delete by UserServiceImpl the User with userId=" + userId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'delete' of UserServiceImpl" +
                    " for  User with userId=" + userId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'delete' of UserServiceImpl" +
                        " for  User with userId=" + userId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'delete' of UserServiceImpl" +
                        " for  User with userId=" + userId);
                logger.error(ex.getMessage(), ex);
            }
        }
    }
}
