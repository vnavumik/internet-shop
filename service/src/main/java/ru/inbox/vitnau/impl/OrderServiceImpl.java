package ru.inbox.vitnau.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.OrderDao;
import ru.inbox.vitnau.dao.connection.ConnectionService;
import ru.inbox.vitnau.dao.impl.OrderDaoImpl;
import ru.inbox.vitnau.dao.model.Order;
import ru.inbox.vitnau.OrderService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);
    private OrderDao orderDao = new OrderDaoImpl();

    @Override
    public List<Order> getAll() {
        logger.info("Try to get by OrderServiceImpl all orders");
        Connection connection = ConnectionService.getInstance().getConnection();
        List<Order> ordersList = new ArrayList<>();
        try {
            connection.setAutoCommit(false);
            ordersList = orderDao.getAll(connection);
            connection.commit();
            logger.info("Return by OrderServiceImpl following orders with orderId:");
            for (Order element : ordersList) {
                logger.info("orderId=" + element.getId());
            }
        } catch (SQLException e) {
            logger.info("Can't get by OrderServiceImpl all orders");
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback  in the method 'getAll' of OrderServiceImpl");
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getAll' of OrderServiceImpl");
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getAll' of OrderServiceImpl");
                logger.error(ex.getMessage(), ex);
            }
        }
        return ordersList;
    }

    @Override
    public List<Order> getByUserId(Long userId) {
        logger.info("Try to get by OrderServiceImpl all orders for User with userId=" + userId);
        Connection connection = ConnectionService.getInstance().getConnection();
        List<Order> ordersList = new ArrayList<>();
        try {
            connection.setAutoCommit(false);
            ordersList = orderDao.getByUserId(connection, userId);
            connection.commit();
            logger.info("Return by OrderServiceImpl for User with userId=" + userId + " following orders " +
                    "with orderId:");
            for (Order element : ordersList) {
                logger.info("orderId=" + element.getId());
            }
            return ordersList;
        } catch (SQLException e) {
            logger.info("Can't get by OrderServiceImpl all orders for User with userId=" + userId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'getByUserId' of OrderServiceImpl" +
                    " for User with userId=" + userId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getByUserId' of OrderServiceImpl " +
                        "for User with userId=" + userId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getByUserId' of OrderServiceImpl " +
                        "for User with userId=" + userId);
                logger.error(ex.getMessage(), ex);
            }
        }
        return ordersList;
    }


    @Override
    public void deleteById(Long orderId) {
        logger.info("Try to delete by OrderServiceImpl the order with orderId=" + orderId);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            orderDao.deleteById(connection, orderId);
            connection.commit();
            logger.info("It was deleted by OrderServiceImpl the order  with orderId=" + orderId);
        } catch (SQLException e) {
            logger.info("Can't delete by OrderServiceImpl thr order with orderId=" + orderId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'deleteById' of OrderServiceImpl for order " +
                    "with orderId=" + orderId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'deleteById' of OrderServiceImpl for order" +
                        " with orderId=" + orderId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback is done in the method 'deleteById' of OrderServiceImpl " +
                        "for order with orderId=" + orderId);
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void save(Order order) {
        Long userId = order.getUser().getId();
        logger.info("Try to save by OrderServiceImpl the order for User with userId= " + userId);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            orderDao.save(connection, order);
            connection.commit();
            logger.info("The order was saved by OrderServiceImpl for User with userId= " + userId);
        } catch (SQLException e) {
            logger.info("Can't save the order by OrderServiceImpl for User with userId= " + userId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'save' of OrderServiceImpl for User with " +
                    "userId= " + userId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'save' of OrderServiceImpl for User with " +
                        "userId= " + userId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'save' of OrderServiceImp for User with " +
                        "userId= " + userId);
                logger.error(ex.getMessage(), ex);
            }
        }
    }
}
