package ru.inbox.vitnau.impl;

import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.util.JAXBParser;
import ru.inbox.vitnau.ParserService;
import ru.inbox.vitnau.model.ProductXML;
import ru.inbox.vitnau.util.ProductXMLConverter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class ParserServiceImpl implements ParserService {

    private static ParserServiceImpl instance = null;

    public static ParserServiceImpl getInstance() {
        if (instance == null) {
            instance = new ParserServiceImpl();
        }
        return instance;
    }

    private ParserServiceImpl() {
    }

    @Override
    public List<Product> parser(File file) {

        JAXBParser jaxbParser = new JAXBParser();
        List<ProductXML> productXMLList = jaxbParser.unmarshall(file);
        List<Product> productList = new ArrayList<>();

        for (ProductXML element : productXMLList) {
            Product product = ProductXMLConverter.toProduct(element);
            productList.add(product);
        }
        return productList;
    }
}
