package ru.inbox.vitnau.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.inbox.vitnau.dao.FeedbackDao;
import ru.inbox.vitnau.dao.connection.ConnectionService;
import ru.inbox.vitnau.dao.impl.FeedbackDaoImpl;
import ru.inbox.vitnau.dao.model.Feedback;
import ru.inbox.vitnau.FeedbackService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FeedbackServiceImpl implements FeedbackService {

    private static final Logger logger = LogManager.getLogger(FeedbackServiceImpl.class);
    private FeedbackDao feedbackDao = new FeedbackDaoImpl();

    @Override
    public List<Feedback> getAll() {
        logger.info("Try to get by FeedbackServiceImpl all feedbacks");
        Connection connection = ConnectionService.getInstance().getConnection();
        List<Feedback> feedbackList = new ArrayList<>();
        try {
            connection.setAutoCommit(false);
            feedbackList = feedbackDao.getAll(connection);
            connection.commit();
            logger.info("Return by FeedbackServiceImpl following feedbacks with feedbackId:");
            for (Feedback element : feedbackList) {
                logger.info("feedbackId=" + element.getId());
            }
        } catch (SQLException e) {
            logger.info("Can't get by FeedbackServiceImpl all feedbacks");
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback  in the method 'getAll' of FeedbackServiceImpl");
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'getAll' of FeedbackServiceImpl");
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'getAll' of FeedbackServiceImpl");
                logger.error(ex.getMessage(), ex);
            }
        }
        return feedbackList;
    }

    @Override
    public void save(Feedback feedback) {
        Long userId = feedback.getUser().getId();
        logger.info("Try to save by FeedbackServiceImpl the feedback for User with userId= " + userId);
        Connection connection = ConnectionService.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
            feedbackDao.save(connection, feedback);
            connection.commit();
            logger.info("The feedback was saved by FeedbackServiceImpl for User with userId= " + userId);
        } catch (SQLException e) {
            logger.info("Can't save the feedback by FeedbackServiceImpl for User with userId= " + userId);
            logger.error(e.getMessage(), e);
            logger.info("Try to do rollback in the method 'save' of FeedbackServiceImpl for User with " +
                    "userId= " + userId);
            try {
                connection.rollback();
                logger.info("Rollback is done in the method 'save' of FeedbackServiceImpl for User with " +
                        "userId= " + userId);
            } catch (SQLException ex) {
                logger.info("Can't do rollback in the method 'save' of FeedbackServiceImpl for User with " +
                        "userId= " + userId);
                logger.error(ex.getMessage(), ex);
            }
        }
    }
}

