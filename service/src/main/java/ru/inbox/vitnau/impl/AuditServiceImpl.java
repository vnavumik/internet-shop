package ru.inbox.vitnau.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.inbox.vitnau.AuditService;
import ru.inbox.vitnau.converter.impl.AuditConverter;
import ru.inbox.vitnau.converter.impl.AuditDTOConverter;
import ru.inbox.vitnau.dao.AuditDao;
import ru.inbox.vitnau.dao.impl.AuditDaoImpl;
import ru.inbox.vitnau.dao.model.Audit;
import ru.inbox.vitnau.dao.util.HibernateUtil;
import ru.inbox.vitnau.dto.AuditDTO;

import java.util.Collections;
import java.util.List;

public class AuditServiceImpl implements AuditService {
    private static final Logger logger = LogManager.getLogger(HibernateUtil.class);

    private AuditDao auditDao = new AuditDaoImpl(Audit.class);
    private AuditDTOConverter auditDTOConverter = new AuditDTOConverter();
    private AuditConverter auditConverter = new AuditConverter();

    @Override
    public List<AuditDTO> getAll() {
        Session session = auditDao.getCurrentSession();

        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Audit> auditList = auditDao.getAll();
            transaction.commit();
            return auditDTOConverter.toDTOList(auditList);
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Fail to fetch audits",e);
        }
        return Collections.emptyList();
    }
}
