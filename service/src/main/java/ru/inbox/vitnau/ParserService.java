package ru.inbox.vitnau;

import ru.inbox.vitnau.dao.model.Product;

import java.io.File;
import java.util.List;

public interface ParserService {

    List<Product> parser(File file);
}
