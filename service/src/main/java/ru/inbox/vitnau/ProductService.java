package ru.inbox.vitnau;

import ru.inbox.vitnau.dao.model.Product;

import java.util.List;

public interface ProductService {

    Integer add(Product product);

    List<Product> getAll();

    Product getById(Long productId);

    Product getByUniqueNumber(Long uniqueNumber);

    void deleteById(Long productId);

}
