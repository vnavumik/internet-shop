package ru.inbox.vitnau;

import ru.inbox.vitnau.dao.model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    List<Order> getByUserId(Long userId);

    void deleteById(Long orderId);

    void save(Order order);
}
