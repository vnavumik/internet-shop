package ru.inbox.vitnau.dto;

import ru.inbox.vitnau.dao.model.User;

public class FeedBackDTO {
    private Long id;
    private User user;
    private String message;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
