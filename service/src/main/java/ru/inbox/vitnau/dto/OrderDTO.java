package ru.inbox.vitnau.dto;

import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.dao.model.User;

public class OrderDTO {
    private Long id;
    private String orderNumber;
    private User user;
    private Product product;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
