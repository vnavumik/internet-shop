package ru.inbox.vitnau;

import ru.inbox.vitnau.dto.AuditDTO;

import java.util.List;

public interface AuditService {

    List<AuditDTO> getAll();


}
