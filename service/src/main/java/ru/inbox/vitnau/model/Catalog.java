package ru.inbox.vitnau.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "catalog")
public class Catalog {

    private List<ProductXML> productXMLList = new ArrayList<>();

    public Catalog() {
    }

    public List<ProductXML> getProductXMLList() {
        return productXMLList;
    }

    @XmlElement(name = "product")
    public void setProductXMLList(List<ProductXML> productXMLList) {
        this.productXMLList = productXMLList;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "productXMLList=" + productXMLList +
                '}';
    }
}
