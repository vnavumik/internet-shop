package ru.inbox.vitnau.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

public class ProductXML {

    private String name;
    private Long uniqueNumber;
    private String description;
    private BigDecimal price;

    public ProductXML() {
    }

    @Override
    public String toString() {
        return "ProductXML{" +
                "name='" + name + '\'' +
                ", uniqueNumber=" + uniqueNumber +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    @XmlAttribute(name = "unique_number")
    public void setUniqueNumber(Long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Long getUniqueNumber() {
        return uniqueNumber;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
