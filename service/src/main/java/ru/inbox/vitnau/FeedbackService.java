package ru.inbox.vitnau;

import ru.inbox.vitnau.dao.model.Feedback;


import java.util.List;

public interface FeedbackService {

    List<Feedback> getAll();

    void save(Feedback feedback);
}
