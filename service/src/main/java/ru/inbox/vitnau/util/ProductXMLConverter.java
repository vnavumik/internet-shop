package ru.inbox.vitnau.util;

import ru.inbox.vitnau.dao.model.Product;
import ru.inbox.vitnau.model.ProductXML;


public class ProductXMLConverter {

    public static Product toProduct(ProductXML productXML) {
        return Product.newBuilder().
                withName(productXML.getName()).
                withUniqueNumber(productXML.getUniqueNumber()).
                withDescription(productXML.getDescription()).
                withPrice(productXML.getPrice()).
                build();
    }
}
