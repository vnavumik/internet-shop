package ru.inbox.vitnau.util;

import ru.inbox.vitnau.model.Catalog;
import ru.inbox.vitnau.model.ProductXML;

import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JAXBParser {

    public List<ProductXML> unmarshall(File file) {

        List<ProductXML> productXMLList = new ArrayList<>();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Catalog catalog = (Catalog) jaxbUnmarshaller.unmarshal(file);
            productXMLList = catalog.getProductXMLList();
        } catch (JAXBException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return productXMLList;
    }
}


